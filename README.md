# Utils

This is a repo where I keep random utilites I've made for various purposed. Typically it's to automate some task.

Language used will vary typically C#/Ts is where I like to play.

Open an issue if you need more info about a utility. I try to make a readme for each utility about what it does and how to use it. 


You can also check out my website [https://calebukle.com](https://calebukle.com) if you want to see other random things I've written about.
