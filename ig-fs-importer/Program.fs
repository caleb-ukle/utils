﻿open System
open System.IO
open FSharp.Data

module InstaHelper =
    let basePath = "/Users/calebukle/SynologyDrive/documents/"
    let mediaPath = basePath + "insta-data/media.json"

    type MediaProvider = JsonProvider<"/Users/calebukle/SynologyDrive/documents/insta-data/media.json">

    let normalizeFileName (badPath: string):string =
        let bad = Path.GetInvalidFileNameChars()
        let p = badPath.Split(bad, StringSplitOptions.RemoveEmptyEntries)
        let clean = String.Concat(p).Replace(" ", "_")
        let endIdx (str: string) =
            if str.Length >= 20 then
               20
            else
                clean.Length
        clean.Substring(0, endIdx clean) + DateTime.Now.Ticks.ToString()

let data = InstaHelper.MediaProvider.Load(InstaHelper.mediaPath)

for p in data.Photos do
    let (s, d) = match p.Caption with
                    | Some c -> (p.Path,p.TakenAt.ToString("/yyyy-MM-dd/") + (InstaHelper.normalizeFileName (c.Replace("/", "")) + ".jpg"))
                    | None -> (p.Path, p.TakenAt.ToString("/yyyy-MM-dd/") + "No-Caption.jpg")
    printfn "copying %s to output %s" s d
    let src = InstaHelper.basePath + "insta-data/" + s
    let dest = InstaHelper.basePath + "formatted" + d

    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest)) |> ignore

    if File.Exists(dest) then
        File.Delete(dest)

    File.Copy(src, dest)