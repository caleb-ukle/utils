# Instagram Importer

This is some code I used to import my Instagram data into my NAS with a specific folder structure.

## Tech Stack
- [Deno](https://deno.land)

## How to use
1. Fill out the `importDir` and `exportDir` variables to point to their respective paths
1. modify the `newPath` you'd like. 
    - my file structure is based on _/YYYY-mm-dd/filename.ext_

> Note I only needed to import my photos as I only had a couple stores and didn't care about DMs or profile pictures. If you want those then you'll need to grab them from the `media.json` file as well, but the same approach can be taken.

You can request you instagram data at any point. 
> Most recent steps will be found [here](https://help.instagram.com/181231772500920).

1. Go to your profile and click the settings icon (gear).
1. Click Privacy and Security.
1. Scroll down to Data Download and click Request Download.
1. Enter the email address where you'd like to receive a link to your data and click Next.
1. Enter your Instagram account password and click Request Download.
1. You'll soon receive an email titled Your Instagram Data with a link to your data. Click Download Data and follow the instructions to finish downloading your information.
