import {dateFmt, Exists} from "./helpers.ts";
console.log(window)

interface IPhotoMetaData {
  caption: string;
  taken_at: string;
  path: string;
}
interface IFormattedMetaData {
  fromPath: string,
  toParentPath: string;
  taken_at: string;
  fileName: string;
}

// path where the instagram data dump is located
const importDir = '/Users/calebukle/SynologyDrive/documents/calebukle_20200602';

// root drive you want to move files into
const exportDir = '/Users/calebukle/SynologyDrive/documents/ig_exports'

const {photos} = JSON.parse(Deno.readTextFileSync('/Users/calebukle/SynologyDrive/documents/calebukle_20200602/media.json'));

const formatted: IFormattedMetaData[] = photos.map(({taken_at, path}: IPhotoMetaData) => {
  const d = new Date(taken_at);
  const parts = path.split('/');
  return {
    fromPath: `${importDir}/${path}`,
    toParentPath: `${exportDir}/${dateFmt(d)}/`,
    fileName: parts[parts.length - 1],
    taken_at,
  }
})

formatted.forEach(({fromPath, toParentPath, fileName}) => {
  if(!Exists(toParentPath)) {
    console.log('Creating new path for', toParentPath);
    Deno.mkdirSync(toParentPath, {recursive: true})
  }

  Deno.copyFileSync(fromPath, `${toParentPath}${fileName}`)
})
