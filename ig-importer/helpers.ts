export const Exists = (filename: string): boolean => {
  try {
    Deno.statSync(filename);
    // successful, file or directory must exist
    return true;
  } catch (error) {
    if (error && error.name === "NotFound") {
      // file or directory does not exist
      return false;
    } else {
      // unexpected error, maybe permissions, pass it along
      throw error;
    }
  }
};


export const dateFmt = (date: Date): string => {

  const parts = [
    date.getFullYear(),
("0"+(date.getMonth() + 1)).slice(-2),
    ("0" + date.getDate()).slice(-2),
  ]

  return parts.join('-')
}
